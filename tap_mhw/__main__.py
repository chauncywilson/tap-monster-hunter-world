"""MHW entry point."""

from __future__ import annotations

from tap_mhw.tap import TapMHW

TapMHW.cli()
