"""Stream type classes for tap-mhw."""

from __future__ import annotations

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_mhw.client import MHWStream

class ArmorStream(MHWStream):
    """Define custom stream."""

    name = "armor"
    path = "/armor"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType, description="The armor's unique identifier"),
        th.Property("slug", th.IntegerType, description="The armor's non displayed name"),
        th.Property("name", th.StringType, description="The armor's name"),
        th.Property("type", th.StringType),
        th.Property("rank", th.StringType),
        th.Property("rarity", th.IntegerType),
        th.Property("defense", th.ObjectType(
            th.Property("base", th.IntegerType),
            th.Property("max", th.IntegerType),
            th.Property("augmented", th.IntegerType),
        )),
        th.Property("resistances", th.ObjectType(
            th.Property("fire", th.IntegerType),
            th.Property("water", th.IntegerType),
            th.Property("thunder", th.IntegerType),
            th.Property("ice", th.IntegerType),
            th.Property("dragon", th.IntegerType),
        )),
        th.Property("slots", th.ArrayType(th.ObjectType(
            th.Property("rank", th.IntegerType),
        ))),
        th.Property("attributes", th.ObjectType(
            th.Property("requiredGender", th.StringType),
        )),
        th.Property("skills", th.ArrayType(th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("level", th.IntegerType),
            th.Property("modifiers", th.ArrayType(th.ObjectType())),
            th.Property("description", th.StringType),
            th.Property("skill", th.IntegerType),
            th.Property("skillName", th.StringType),
        ))),
        th.Property("armorSet", th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("rank", th.StringType),
            th.Property("name", th.StringType),
            th.Property("pieces", th.IntegerType),
            th.Property("bonus", th.StringType),
        )),
        th.Property("assets", th.ObjectType(
            th.Property("imageMale", th.StringType),
            th.Property("imageFemale", th.StringType),
        )),
        th.Property("crafting", th.ObjectType(
            th.Property("materials", th.ArrayType(th.ObjectType(
                th.Property("quantity", th.IntegerType),
                th.Property("item", th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("rarity", th.IntegerType),
                    th.Property("carryLimit", th.IntegerType),
                    th.Property("value", th.IntegerType),
                    th.Property("name", th.StringType),
                    th.Property("description", th.StringType),
            ))),
            )),
        )),
    ).to_dict()


class WeaponsStream(MHWStream):
    """Define custom stream."""

    name = "weapons"
    path = "/weapons"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("type", th.StringType),
        th.Property("rarity", th.IntegerType),
        th.Property("attack", th.ObjectType(
            th.Property("display", th.IntegerType),
            th.Property("raw", th.IntegerType),
        )),
        th.Property("elderseal", th.StringType),
        th.Property("attributes", th.ObjectType(
            th.Property("affinity", th.IntegerType),
            th.Property("defense", th.IntegerType),
            th.Property("boostType", th.StringType),
            th.Property("damageType", th.StringType),
            th.Property("coatings", th.ArrayType(th.StringType)),
            th.Property("elderseal", th.StringType),
        )),
        th.Property("damageType", th.StringType),
        th.Property("name", th.DateTimeType),
        th.Property("shelling", th.ObjectType(
            th.Property("type", th.StringType),
            th.Property("level", th.IntegerType),
        )),
        th.Property("specialAmmo", th.StringType),
        th.Property("deviation", th.StringType),
        th.Property("ammo", th.ArrayType(th.ObjectType(
            th.Property("type", th.StringType),
            th.Property("capacities", th.ArrayType(th.IntegerType)),
        ))),
        th.Property("coatings", th.ArrayType(th.StringType)),
        th.Property("durability", th.ArrayType(th.ObjectType(
            th.Property("red", th.IntegerType),
            th.Property("orange", th.IntegerType),
            th.Property("yellow", th.IntegerType),
            th.Property("green", th.IntegerType),
            th.Property("blue", th.IntegerType),
            th.Property("white", th.IntegerType),
            th.Property("purple", th.IntegerType),
        ))),
        th.Property("phial", th.ObjectType(
            th.Property("type", th.StringType),
            th.Property("damage", th.IntegerType),
        )),
        th.Property("boostType", th.StringType),
        th.Property("slots", th.ArrayType(th.ObjectType(
            th.Property("rank", th.IntegerType),
        ))),
        th.Property("elements", th.ArrayType(th.ObjectType(
            th.Property("type", th.StringType),
            th.Property("damage", th.IntegerType),
            th.Property("hidden", th.BooleanType),
        ))),
        th.Property("crafting", th.ObjectType(
            th.Property("craftable", th.BooleanType),
            th.Property("previous", th.StringType),
            th.Property("branches", th.ArrayType(th.IntegerType)),
            th.Property("craftingMaterials", th.ArrayType(th.ObjectType(
                th.Property("quantity", th.IntegerType),
                th.Property("item", th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("rarity", th.IntegerType),
                    th.Property("carryLimit", th.IntegerType),
                    th.Property("value", th.IntegerType),
                    th.Property("name", th.StringType),
                    th.Property("description", th.StringType),
                ))),
            )),
            th.Property("upgradeMaterials", th.ArrayType(th.ObjectType(
                th.Property("quantity", th.IntegerType),
                th.Property("item", th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("rarity", th.IntegerType),
                    th.Property("carryLimit", th.IntegerType),
                    th.Property("value", th.IntegerType),
                    th.Property("name", th.StringType),
                    th.Property("description", th.StringType),
            ))),
        )))),
        th.Property("assets", th.ObjectType(
            th.Property("icon", th.StringType),
            th.Property("image", th.StringType),
        )),
    ).to_dict()

class MonsterStream(MHWStream):
    """Define custom stream."""

    name = "monsters"
    path = "/monsters"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("type", th.StringType),
        th.Property("name", th.StringType),
        th.Property("species", th.StringType),
        th.Property("description", th.StringType),
        th.Property("ailments", th.ArrayType(th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("description", th.StringType),
            th.Property("recovery", th.ObjectType(
                th.Property("actions", th.ArrayType(th.StringType)),
                th.Property("items", th.ArrayType(th.StringType)),
            )),
            th.Property("protection", th.ObjectType(
                th.Property("skills", th.ArrayType(th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("name", th.StringType),
                    th.Property("level", th.IntegerType),
                    th.Property("modifiers", th.ArrayType(th.ObjectType())),
                    th.Property("description", th.StringType),
                    th.Property("skill", th.IntegerType),
                    th.Property("skillName", th.StringType),
                ))),
                th.Property("items", th.ArrayType(th.StringType)),
            )),
        ))),
        th.Property("size", th.StringType),
        th.Property("elements", th.ArrayType(th.ObjectType(
            th.Property("type", th.StringType),
            th.Property("damage", th.IntegerType),
            th.Property("toughness", th.StringType),
        ))),
        th.Property("locations", th.ArrayType(th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("zoneCount", th.IntegerType),
        ))),
        th.Property("resistances", th.ObjectType(
            th.Property("fire", th.StringType),
            th.Property("water", th.StringType),
            th.Property("thunder", th.StringType),
            th.Property("ice", th.StringType),
            th.Property("dragon", th.StringType),
        )),
        th.Property("weaknesses", th.ArrayType(th.ObjectType(
            th.Property("element", th.StringType),
            th.Property("stars", th.IntegerType),
            th.Property("condition", th.StringType),
        ))),
        th.Property("rewards", th.ObjectType(
            th.Property("lowRank", th.ArrayType(th.ObjectType(
                th.Property("item", th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("rarity", th.IntegerType),
                    th.Property("carryLimit", th.IntegerType),
                    th.Property("value", th.IntegerType),
                    th.Property("name", th.StringType),
                    th.Property("description", th.StringType),
                )),
                th.Property("quantity", th.IntegerType),
                th.Property("percentage", th.IntegerType),
            ))),
            th.Property("highRank", th.ArrayType(th.ObjectType(
                th.Property("item", th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("rarity", th.IntegerType),
                    th.Property("carryLimit", th.IntegerType),
                    th.Property("value", th.IntegerType),
                    th.Property("name", th.StringType),
                    th.Property("description", th.StringType),
                )),
                th.Property("quantity", th.IntegerType),
                th.Property("percentage", th.IntegerType),
            ))),
            th.Property("tempered", th.ArrayType(th.ObjectType(
                th.Property("item", th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("rarity", th.IntegerType),
                    th.Property("carryLimit", th.IntegerType),
                    th.Property("value", th.IntegerType),
                    th.Property("name", th.StringType),
                    th.Property("description", th.StringType),
                )),
                th.Property("quantity", th.IntegerType),
                th.Property("percentage", th.IntegerType),
            ))),
        )),
        th.Property("assets", th.ObjectType(
            th.Property("icon", th.StringType),
            th.Property("image", th.StringType),
        )),
    ).to_dict()

class CharmStream(MHWStream) :
    name = "charms"
    path = "/charms"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("ranks", th.ArrayType(th.ObjectType(
            th.Property("level", th.IntegerType),
            th.Property("rarity", th.IntegerType),
            th.Property("name", th.StringType),
            th.Property("skills", th.ArrayType(th.ObjectType(
                th.Property("id", th.IntegerType),
                th.Property("level", th.IntegerType),
                th.Property("modifiers", th.ArrayType(th.ObjectType())),
                th.Property("description", th.StringType),
                th.Property("skill", th.IntegerType),
                th.Property("skillName", th.StringType),
            ))),
            th.Property("crafting", th.ObjectType(
                th.Property("craftable", th.BooleanType),
                th.Property("materials", th.ArrayType(th.ObjectType(
                    th.Property("quantity", th.IntegerType),
                    th.Property("item", th.ObjectType(
                        th.Property("id", th.IntegerType),
                        th.Property("rarity", th.IntegerType),
                        th.Property("carryLimit", th.IntegerType),
                        th.Property("value", th.IntegerType),
                        th.Property("name", th.StringType),
                        th.Property("description", th.StringType),
                    ))
                )))
            ))
        ))),
        th.Property("rarity", th.IntegerType),
        th.Property("skills", th.ArrayType(th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("level", th.IntegerType),
            th.Property("modifiers", th.ArrayType(th.ObjectType()),
            th.Property("description", th.StringType),
            th.Property("skill", th.IntegerType),
            th.Property("skillName", th.StringType),
        )))),
        th.Property("crafting", th.ObjectType(
            th.Property("materials", th.ArrayType(th.ObjectType(
                th.Property("quantity", th.IntegerType),
                th.Property("item", th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("rarity", th.IntegerType),
                    th.Property("carryLimit", th.IntegerType),
                    th.Property("value", th.IntegerType),
                    th.Property("name", th.StringType),
                    th.Property("description", th.StringType),
                )),
            )),
        ))),
        th.Property("assets", th.ObjectType(
            th.Property("icon", th.StringType),
            th.Property("image", th.StringType),
        )),
    ).to_dict()

class ItemStream(MHWStream) :
    name = "items"
    path = "/items"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("rarity", th.IntegerType),
        th.Property("carryLimit", th.IntegerType),
        th.Property("value", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("description", th.StringType),
    ).to_dict()

class SkillStream(MHWStream) :
    name = "skills"
    path = "/skills"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("description", th.StringType),
        th.Property("ranks", th.ArrayType(th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("skill", th.IntegerType),
            th.Property("level", th.IntegerType),
            th.Property("modifiers", th.ObjectType(
                th.Property("attack", th.IntegerType),
                th.Property("affinity", th.IntegerType),
                th.Property("defense", th.IntegerType),
                th.Property("resistAll", th.IntegerType),
                th.Property("health", th.IntegerType),
                th.Property("resistFire", th.IntegerType),
                th.Property("resistWater", th.IntegerType),
                th.Property("resistThunder", th.IntegerType),
                th.Property("resistIce", th.IntegerType),
                th.Property("resistDragon", th.IntegerType),
                th.Property("damageFire", th.IntegerType),
                th.Property("damageWater", th.IntegerType),
                th.Property("damageThunder", th.IntegerType),
                th.Property("damageIce", th.IntegerType),
                th.Property("damageDragon", th.IntegerType),
                th.Property("sharpnessBonus", th.IntegerType),
            )),
            th.Property("description", th.StringType),
            th.Property("skillName", th.StringType),
        ))),
    ).to_dict()