"""MHW tap class."""

from __future__ import annotations

from singer_sdk import Tap
from singer_sdk import typing as th  # JSON schema typing helpers

from tap_mhw import streams
streams.ArmorStream
streams.WeaponsStream
streams.MonsterStream
streams.CharmStream
streams.ItemStream
streams.SkillStream


class TapMHW(Tap):
    """MHW tap class."""

    name = "tap-mhw"

    def discover_streams(self) -> list[streams.MHWStream]:
        """Return a list of discovered streams.

        Returns:
            A list of discovered streams.
        """
        return [
            streams.ArmorStream(self),
            streams.WeaponsStream(self),
            streams.MonsterStream(self),
            streams.CharmStream(self),
            streams.ItemStream(self),
            streams.SkillStream(self),
        ]


if __name__ == "__main__":
    TapMHW.cli()
